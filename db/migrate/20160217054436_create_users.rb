class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.integer :title_id
      t.integer :user_status_id
      t.string :fname
      t.string :lname
      t.string :phone
      t.string :email
      t.string :address
      t.string :state
      t.string :city
      t.text :comment

      t.timestamps null: false
    end
  end
end
