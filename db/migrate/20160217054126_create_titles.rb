class CreateTitles < ActiveRecord::Migration
  def change
    create_table :titles do |t|
      t.string :position
      t.text :comment

      t.timestamps null: false
    end
  end
end
