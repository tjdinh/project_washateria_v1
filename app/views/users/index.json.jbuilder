json.array!(@users) do |user|
  json.extract! user, :id, :title_id, :user_status_id, :fname, :lname, :phone, :email, :address, :state, :city, :comment
  json.url user_url(user, format: :json)
end
