json.array!(@titles) do |title|
  json.extract! title, :id, :position, :comment
  json.url title_url(title, format: :json)
end
