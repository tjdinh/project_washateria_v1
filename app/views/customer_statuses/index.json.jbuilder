json.array!(@customer_statuses) do |customer_status|
  json.extract! customer_status, :id, :description
  json.url customer_status_url(customer_status, format: :json)
end
