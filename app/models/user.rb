class User < ActiveRecord::Base
  belongs_to :user_status
  belongs_to :title

  #method
  def Full_User_Name
    "#{fname} #{lname}"
  end

  def fname=(s)
    super s.titleize
  end
  def lname=(s)
    super s.titleize
  end
  def address=(s)
    super s.titleize
  end
  def city=(s)
    super s.titleize
  end

  #validation
  validates :fname, :lname, :address, :city,
            presence:true
  validates :phone,
            length: {minimum: 10, maximum: 11},
            numericality: true
  validates :fname, :lname,
            format: {with: /\A[a-zA-Z]+\z/}
  validates :city,
            length: {maximum:  22}
  validates_format_of :email,
                      :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, :on => :create

end
